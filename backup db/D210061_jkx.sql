-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Окт 07 2021 г., 16:34
-- Версия сервера: 10.3.31-MariaDB-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `D210061_jkx`
--

-- --------------------------------------------------------

--
-- Структура таблицы `payments`
--

CREATE TABLE `payments` (
  `personal_account_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `summ` decimal(10,2) NOT NULL,
  `payment_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `payments`
--

INSERT INTO `payments` (`personal_account_id`, `service_id`, `month`, `year`, `summ`, `payment_date`) VALUES
(1, 2, 3, 2021, '400.00', '2021-10-05 17:30:27'),
(1, 3, 3, 2021, '200.00', '2021-10-05 17:30:27'),
(1, 4, 3, 2021, '300.60', '2021-10-05 17:30:27'),
(2, 2, 4, 2021, '542.00', '2021-10-05 17:30:27'),
(2, 3, 4, 2021, '210.53', '2021-10-05 17:30:27'),
(2, 4, 4, 2021, '350.60', '2021-10-05 17:30:27');

-- --------------------------------------------------------

--
-- Структура таблицы `personal_accounts`
--

CREATE TABLE `personal_accounts` (
  `id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `father_name` varchar(100) NOT NULL,
  `ploshad` float NOT NULL,
  `chislenost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `personal_accounts`
--

INSERT INTO `personal_accounts` (`id`, `address`, `last_name`, `first_name`, `father_name`, `ploshad`, `chislenost`) VALUES
(1, 'г. Иркутск, ул. Ленина 5а', 'ивано', 'Сергей', 'Леонидович', 62, 3),
(2, 'г. Иркутск, ул. Барикад 147', 'Петров', 'Данил', 'Александрович', 25, 1),
(3, 'г. Иркутск, ул. Советская 2А', 'Сидоров', 'Константин', 'Александрович', 47.6, 2),
(12, '8380 Eu Ave', 'Jennifer Fitzpatrick', 'Jordan Bell', 'Nell Baird', 4, 9),
(13, '674-4221 Nec, Avenue', 'Jesse Klein', 'Jackson Matthews', 'Carl Faulkner', 6, 6),
(14, '684-6404 Vulputate Av.', 'Lewis Wheeler', 'Melinda Phillips', 'Craig Cantu', 2, 1),
(15, 'Ap #806-5625 Rhoncus. Rd.', 'Brooke Willis', 'Demetrius Barr', 'McKenzie Olson', 4, 5),
(16, '177-7440 Pellentesque Rd.', 'Abigail Wheeler', 'Madeson Hawkins', 'Regina Knight', 3, 1),
(17, 'P.O. Box 717, 2507 Facilisis, Ave', 'Alice Riddle', 'Damon Madden', 'Kareem Lloyd', 6, 5),
(18, '3935 Dolor. St.', 'Willa Lawrence', 'Seth Mckenzie', 'Giselle Martinez', 7, 7),
(19, '4347 Et Ave', 'Yasir Haley', 'Jayme Ray', 'Jacqueline Dillon', 7, 8),
(20, 'Ap #776-6139 Eget, Av.', 'Orson Stevenson', 'Nolan Forbes', 'Lacota Fox', 4, 8),
(21, '6123 Sit Ave', 'Callie Dodson', 'Amela Romero', 'Jameson Buchanan', 4, 4),
(22, 'Ap #870-8890 Morbi Av.', 'Silas Bean', 'Cassidy Forbes', 'Stone Larson', 4, 8),
(23, 'Ap #471-7014 Nonummy Av.', 'Benjamin Mcpherson', 'Daryl Rosa', 'Danielle Crawford', 2, 0),
(24, '783-4398 Sem. Ave', 'Sybil Blake', 'Elmo Finch', 'Zeph Le', 7, 8),
(25, 'P.O. Box 319, 3640 Vitae Av.', 'Coby Pollard', 'Travis Cannon', 'Claire Fernandez', 0, 8),
(26, '3238 Dapibus Rd.', 'Jack Johnson', 'Ruth Kaufman', 'Cheryl Bennett', 0, 8),
(27, '733-4108 Et Av.', 'Whoopi Franco', 'Bertha Graham', 'Kim Sparks', 3, 7),
(28, 'Ap #376-7884 Vitae, Rd.', 'Olympia Wallace', 'Isaac Wynn', 'Kylie Baldwin', 3, 3),
(29, 'Ap #354-4560 Egestas St.', 'Alexander Bowen', 'Leilani Duncan', 'Teegan Buckner', 2, 5),
(30, '6797 Non St.', 'Yael James', 'Hayes Rutledge', 'Jorden Stone', 3, 8),
(31, 'Ap #167-9302 Lorem Avenue', 'Kamal Herrera', 'Rhona Battle', 'Cruz Cash', 6, 2),
(32, '317-1194 Nullam Av.', 'Heidi Avery', 'Jarrod Workman', 'Malik Salas', 2, 7),
(33, 'Ap #226-8669 Lobortis, Ave', 'Dana Witt', 'Talon Alvarado', 'Blossom Gilliam', 9, 7),
(34, 'Ap #116-5250 Scelerisque Rd.', 'Steven Downs', 'Hanna O\'brien', 'Keefe Dodson', 9, 10),
(35, '657-9917 Quis St.', 'Yetta Peterson', 'Buckminster Bowers', 'Neville Benjamin', 5, 7),
(36, '249-6360 Quam Ave', 'Honorato Perez', 'Evangeline Dennis', 'Dominic Byers', 3, 5),
(37, '1175 Luctus Road', 'Valentine Garrison', 'Lev Burns', 'Cecilia Chaney', 8, 0),
(38, '437-7378 Quis St.', 'Kameko Meyer', 'Blake Rowland', 'Xantha Gallagher', 4, 4),
(39, 'P.O. Box 650, 4753 Lorem Avenue', 'Ulla Cote', 'Jack Terrell', 'Mira Roman', 6, 6),
(40, 'Ap #229-3610 Pellentesque Road', 'August Harper', 'Willa Gregory', 'Odessa Cook', 6, 7),
(41, 'P.O. Box 857, 359 Velit Road', 'Rylee Cooley', 'Alexander Dorsey', 'Geraldine Dean', 2, 1),
(42, 'P.O. Box 234, 6165 Egestas Street', 'Lavinia Hatfield', 'Olga Johns', 'Aurelia Peterson', 4, 4),
(43, 'P.O. Box 636, 8878 Aliquet, Rd.', 'Regina Hernandez', 'Keely Spears', 'Lee Sutton', 5, 0),
(44, 'Ap #841-9262 At, Rd.', 'Lysandra Taylor', 'Vaughan Sellers', 'Hedda Velez', 9, 6),
(45, '490-6545 Aliquet, St.', 'Virginia Stevens', 'Hayfa Cherry', 'Aaron Tucker', 6, 5),
(46, 'P.O. Box 972, 417 Non, Ave', 'Kathleen Richmond', 'Lunea Estrada', 'Ima Levy', 6, 6),
(47, '3858 Neque Ave', 'Madeson Mueller', 'Elton Sweet', 'Rogan Stanton', 7, 1),
(48, '6105 Duis Rd.', 'Kenyon Roy', 'Laura Burgess', 'Christopher Whitney', 4, 4),
(49, '5562 Interdum. Street', 'Alika Duffy', 'Irene Hahn', 'Rowan Browning', 6, 1),
(50, 'Ap #747-6281 Diam. Ave', 'Brianna Riley', 'Daria Winters', 'Amir Diaz', 2, 9),
(51, '8438 Natoque Road', 'Clementine Buchanan', 'Sacha Daniels', 'Nina Blair', 4, 2),
(52, '342-6409 Cras Street', 'Axel Mcdonald', 'Howard Harrell', 'Ashton Holder', 7, 10),
(53, 'Ap #536-8780 Molestie St.', 'Katell Vance', 'Aiko Spears', 'Cynthia Barr', 0, 6),
(54, 'Ap #797-4844 Consequat St.', 'Celeste Browning', 'Vanna Ratliff', 'Kameko Wolf', 3, 9),
(55, '345-9735 Lobortis Rd.', 'MacKensie Cruz', 'Gareth Barlow', 'Quynn Salas', 8, 4),
(56, '934-8105 Dui. Av.', 'Cameran Puckett', 'Zorita Bryan', 'Althea Rojas', 8, 0),
(57, '896-1289 Ultrices Ave', 'Otto Barnett', 'Dale Harrison', 'Evan Blankenship', 1, 8),
(58, '267-9779 Neque Avenue', 'Marvin Peterson', 'Dustin Hancock', 'Holly Malone', 3, 3),
(59, 'P.O. Box 110, 6414 Velit. Street', 'Freya Kent', 'Xyla Pittman', 'Ayanna Goodwin', 5, 10),
(60, 'P.O. Box 267, 8784 Pede. Street', 'Rina Sanford', 'Kirk Fisher', 'Lysandra Camacho', 6, 2),
(61, '901-1923 Duis Avenue', 'Kessie Gallegos', 'Ingrid Koch', 'Herman Mccray', 2, 3),
(62, '4587 Diam. Street', 'May Sutton', 'Justin Reeves', 'Karyn Alexander', 7, 5),
(63, 'P.O. Box 374, 5662 Nisi. Av.', 'Mari Bennett', 'Lars Melton', 'Lev Meyer', 9, 5),
(64, '462-8112 Cras Rd.', 'Elvis Freeman', 'Victor Francis', 'Cullen O\'connor', 9, 5),
(65, 'P.O. Box 591, 4258 In Rd.', 'Nehru Craft', 'Fritz Allen', 'Breanna Hyde', 6, 2),
(66, 'P.O. Box 980, 2426 Pellentesque Street', 'Nevada Stark', 'Grant Anderson', 'Holmes Tate', 8, 5),
(67, 'Ap #819-897 Vivamus St.', 'Galena Woods', 'Tanek Gillespie', 'Thor Hayden', 1, 3),
(68, 'P.O. Box 621, 198 Proin Av.', 'Beverly Vance', 'Igor Hendrix', 'Burton Mcconnell', 4, 9),
(69, 'P.O. Box 483, 8083 Libero. Rd.', 'Kelsie Beach', 'Beverly Leach', 'Cole Morin', 2, 9),
(70, '5913 Est St.', 'Griffin Merritt', 'Ella Trevino', 'Craig Evans', 1, 1),
(71, '423-7939 Eu Ave', 'Dahlia Floyd', 'Iola Newman', 'Noelani Long', 7, 7),
(72, 'P.O. Box 291, 4927 Tempor Rd.', 'Robin Horton', 'Noah Gilmore', 'Ginger Osborne', 4, 1),
(73, '2438 Mollis. Rd.', 'Quentin Franks', 'Joseph Miles', 'Sebastian Sosa', 10, 5),
(74, 'Ap #671-8959 In St.', 'Deirdre Dyer', 'Nomlanga Estrada', 'Jacob Moody', 1, 6),
(75, 'P.O. Box 888, 9908 Sed, Ave', 'Derek Gillespie', 'Nathaniel Lindsey', 'Cole Holman', 6, 1),
(76, 'Ap #549-145 Velit. Rd.', 'Vanna Harrell', 'Lars Reyes', 'Shad Briggs', 4, 9),
(77, '966-1816 Et, Avenue', 'Barbara Stevenson', 'Oscar Goff', 'Malik Keith', 4, 4),
(78, '204 Cras St.', 'Wade Bishop', 'Hamilton Boyd', 'Ifeoma Bryant', 9, 3),
(79, 'Ap #916-407 Mauris Street', 'Winifred Strickland', 'Fitzgerald Torres', 'Ayanna Hopkins', 4, 9),
(80, 'Ap #733-156 Metus Avenue', 'Julie Marsh', 'Karleigh Sheppard', 'Meghan Bullock', 6, 7),
(81, 'Ap #743-382 Sem Street', 'Yardley Best', 'Ruby Reeves', 'Amal Cherry', 5, 1),
(82, 'P.O. Box 455, 9290 Dui, Road', 'Rashad Holmes', 'Aaron Simpson', 'Abbot Mcgowan', 8, 1),
(83, 'Ap #429-4960 Ultricies St.', 'Rashad Osborne', 'Nero Hicks', 'Leo Noel', 1, 7),
(84, '6896 Egestas Rd.', 'Evangeline Howell', 'Melanie Faulkner', 'Zelda Foreman', 9, 0),
(85, '331-597 Pharetra. Road', 'Charde Alford', 'Tallulah Blackburn', 'Oren Park', 1, 4),
(86, 'Ap #158-9869 Nulla. Street', 'Denton Gamble', 'Stella Brennan', 'Lesley Pitts', 6, 8),
(87, 'Ap #121-9368 Nulla Av.', 'Marcia Sutton', 'Graham Michael', 'Neil Mcconnell', 9, 1),
(88, 'P.O. Box 289, 2488 Hendrerit St.', 'Leila Marsh', 'Peter Whitney', 'Madeline Baxter', 2, 7),
(89, '860-1509 Ipsum. St.', 'Adrienne Franks', 'Galena Hull', 'Kyra Cotton', 6, 6),
(90, '4865 Fringilla Avenue', 'Emery Cash', 'Chaim Pugh', 'Moana Aguilar', 8, 10),
(91, 'P.O. Box 275, 6652 Sem. St.', 'Kimberley Jarvis', 'Vivian Flores', 'Solomon Savage', 4, 8),
(92, 'Ap #568-8536 Ac Rd.', 'Emily Luna', 'Alyssa Russell', 'Fallon Randall', 6, 9),
(93, '268-7025 Aliquet Ave', 'Robert Sears', 'Kalia Nelson', 'Ifeoma Thornton', 1, 8),
(94, 'Ap #406-5322 Massa. Rd.', 'Chloe Moran', 'Davis Sexton', 'Ray Torres', 6, 4),
(95, 'P.O. Box 910, 9382 Velit. St.', 'Bernard Dyer', 'Alfreda Walton', 'Claudia Downs', 2, 8),
(96, '959-3170 Tempus St.', 'Doris Mcmahon', 'Risa Lott', 'Mariam Bond', 0, 2),
(97, 'Ap #466-1343 Vestibulum, St.', 'Kelsey Buckley', 'Oren Pearson', 'Brady Kirkland', 5, 1),
(98, 'Ap #459-1111 Quisque Avenue', 'Randall Kirby', 'Lillith Underwood', 'Maile Conway', 7, 7),
(99, 'Ap #125-1154 Convallis Ave', 'Marshall Randolph', 'Halla Castillo', 'Deacon Oneal', 6, 3),
(100, 'Ap #436-9756 Ornare St.', 'Brynne Dominguez', 'Ali Steele', 'Stacey Massey', 10, 6),
(101, 'P.O. Box 669, 469 Ligula Av.', 'Phelan Gonzalez', 'Keelie Cline', 'Gregory Macias', 1, 1),
(102, '875-2603 Amet Street', 'Lani Johnston', 'Iris Harris', 'Maya Whitehead', 10, 4),
(103, '852-6657 Lacus. Av.', 'Oliver Carrillo', 'Josephine Salazar', 'Michael Hogan', 9, 6),
(104, '7505 Integer St.', 'Brett O\'neill', 'Ria Espinoza', 'Callie Benton', 0, 6),
(105, '205-9838 Vel St.', 'Brian Sexton', 'Maisie Hoffman', 'Fritz Byrd', 9, 3),
(106, '449-9866 Vitae, Avenue', 'Jarrod Rowland', 'Russell Crosby', 'Ivy Johns', 4, 7),
(107, '6651 Dolor Road', 'Erin O\'donnell', 'Whoopi Crawford', 'Octavia Browning', 7, 6),
(108, '314-9540 Praesent Rd.', 'Jelani Rosales', 'Benedict Hunt', 'Inga Arnold', 5, 8),
(109, 'Ap #767-8278 Elit. St.', 'Elliott Holland', 'Uma Clay', 'Julie Callahan', 8, 2),
(110, '437-5466 Malesuada. St.', 'Kelly Castillo', 'Megan Ballard', 'Stuart Collier', 6, 2),
(111, 'Ap #883-2989 Aliquet Rd.', 'Shay Bauer', 'Ali George', 'Cora Hahn', 1, 7);

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`id`, `name`, `price`) VALUES
(2, 'Тепло', '20.00'),
(3, 'Свет', '1.20'),
(4, 'ТБО', '160.00'),
(18, 'Tyrone Walsh', '7.00'),
(19, 'Myra Guerrero', '6.00'),
(20, 'Yeo Keith', '0.00'),
(21, 'Zachery Flowers', '0.00'),
(22, 'Denise Mckee', '7.00'),
(23, 'Mason Simmons', '7.00'),
(24, 'Karina Ryan', '4.00'),
(25, 'Jordan Padilla', '3.00'),
(26, 'Geoffrey Bass', '9.00'),
(27, 'Russell Duffy', '0.00'),
(28, 'Arsenio Cole', '2.00'),
(29, 'Ava Molina', '10.00'),
(30, 'Harriet Mclaughlin', '7.00'),
(31, 'Gabriel Brown', '5.00'),
(32, 'Reece Deleon', '2.00'),
(33, 'Jin Gould', '2.00'),
(34, 'Ivor Marquez', '10.00'),
(35, 'Moses Burgess', '9.00'),
(36, 'Colt Dodson', '9.00'),
(37, 'Lynn Byrd', '2.00'),
(38, 'Byron Jennings', '10.00'),
(39, 'Chadwick Cox', '3.00'),
(40, 'Donovan Page', '4.00'),
(41, 'Hall Conrad', '6.00'),
(42, 'Jordan Rasmussen', '5.00'),
(43, 'Thaddeus Mann', '7.00'),
(44, 'Maisie Scott', '5.00'),
(45, 'Christian Harrell', '10.00'),
(46, 'Fleur Ferrell', '2.00'),
(47, 'Mira Washington', '8.00'),
(48, 'Ivor Cruz', '8.00'),
(49, 'Lyle Ingram', '7.00'),
(50, 'Finn Tucker', '1.00'),
(51, 'Tallulah Townsend', '3.00'),
(52, 'Raphael Watson', '1.00'),
(53, 'Howard Gomez', '8.00'),
(54, 'Jana Matthews', '8.00'),
(55, 'Erasmus Skinner', '10.00'),
(56, 'Rebecca Combs', '5.00'),
(57, 'Fulton Maxwell', '6.00'),
(58, 'Paki Ramirez', '5.00'),
(59, 'Buckminster Cantu', '4.00'),
(60, 'Medge Avila', '7.00'),
(61, 'Cullen Hancock', '0.00'),
(62, 'Gail Holmes', '8.00'),
(63, 'Lee King', '6.00'),
(64, 'Aurora Osborn', '6.00'),
(65, 'Tyrone Mooney', '2.00'),
(66, 'Caleb Shepherd', '3.00'),
(67, 'Tucker Horne', '3.00'),
(68, 'Sebastian Beard', '3.00'),
(69, 'Chloe Winters', '2.00'),
(70, 'Chaim Pruitt', '1.00'),
(71, 'Jordan Christian', '7.00'),
(72, 'Wayne Franco', '2.00'),
(73, 'Amaya Noel', '9.00'),
(74, 'Hunter Lamb', '6.00'),
(75, 'Kasimir Schultz', '6.00'),
(76, 'Chancellor Goodman', '4.00'),
(77, 'Garth Lynn', '3.00'),
(78, 'Calista Leach', '6.00'),
(79, 'Armando Vazquez', '2.00'),
(80, 'Allistair Patterson', '2.00'),
(81, 'Avye Payne', '9.00'),
(82, 'Bert Neal', '3.00'),
(83, 'McKenzie Strong', '5.00'),
(84, 'Alfonso Gilliam', '6.00'),
(85, 'Carol Suarez', '10.00'),
(86, 'Kenyon Acevedo', '2.00'),
(87, 'Rowan Ray', '8.00'),
(88, 'Wynne Atkinson', '6.00'),
(89, 'Sarah Roman', '3.00'),
(90, 'Aretha Hammond', '6.00'),
(91, 'Bo Mercado', '0.00'),
(92, 'Myra Levine', '2.00'),
(93, 'Daryl Hodge', '0.00'),
(94, 'Martena Tran', '4.00'),
(95, 'Hanae Noel', '6.00'),
(96, 'Doris Foster', '5.00'),
(97, 'Merrill Hernandez', '8.00'),
(98, 'Vivian Dudley', '8.00'),
(99, 'Christian Weber', '6.00'),
(100, 'Meghan Bruce', '6.00'),
(101, 'Silas Reed', '6.00'),
(102, 'Christian Dodson', '9.00'),
(103, 'Evelyn Petersen', '3.00'),
(104, 'Kyla Gonzales', '8.00'),
(105, 'Anthony Malone', '4.00'),
(106, 'Cruz Eaton', '4.00'),
(107, 'Reuben Berg', '8.00'),
(108, 'Olympia Watson', '3.00'),
(109, 'Ignatius Merrill', '9.00'),
(110, 'Cole Castaneda', '8.00'),
(111, 'Joy Pope', '0.00'),
(112, 'Chantale Dunn', '7.00'),
(113, 'Giselle Bryant', '3.00'),
(114, 'Ina Bullock', '7.00'),
(115, 'Hanna Walton', '6.00'),
(116, 'Gavin Miranda', '1.00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`personal_account_id`,`service_id`),
  ADD KEY `service_id` (`service_id`);

--
-- Индексы таблицы `personal_accounts`
--
ALTER TABLE `personal_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `personal_accounts`
--
ALTER TABLE `personal_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT для таблицы `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_ibfk_2` FOREIGN KEY (`personal_account_id`) REFERENCES `personal_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
