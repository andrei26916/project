<?php

require_once "../service/Main.php";
require_once "../views/layout/success.php";

if ($_SESSION['role'] == 'admin') {
    (new Main())->createPersonalAccount($_GET['address'], $_GET['last_name'], $_GET['first_name'], $_GET['father_name'], $_GET['ploshad'], $_GET['chislenost']);

    header('Refresh: 2; url=/views/account.php');
}

?>
