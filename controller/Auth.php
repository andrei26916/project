<?php

session_start();

if (isset($_SESSION['role']))
{
    unset($_SESSION['role']);
    header('Location: /');
}

$_SESSION['role'] = $_GET['role'];
header('Location: /');