<?php
require_once "../service/Main.php";
require_once "../views/layout/success.php";

if ($_SESSION['role'] == 'admin') {

    (new Main())->createService($_GET['name'], $_GET['price']);

    header('Refresh: 2; url=/views/service.php');

}
?>
