<?php

session_start();
require_once "../service/Main.php";

if ($_SESSION['role'] == 'admin') {
    return (new Main())->deletePersonalAccount($_GET['id']);
}
