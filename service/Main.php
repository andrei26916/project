<?php


class Main
{

    private $db;

    /**
     * Main constructor.
     */
    public function __construct()
    {
        $this->db = new PDO('mysql:host=10.100.3.80;dbname=D210061_jkx', 'D210061@iat.iat', 'rljwof');
    }

    /**
     * @param string $name
     * @param float $price
     * @return bool
     */
    public function createService(string $name, float $price): bool
    {
        $result = $this->db->prepare("INSERT INTO services (name, price) VALUES (:name, :price)");
        return $result->execute([
            ':name' => $name,
            ':price' => $price
        ]);
    }

    /**
     * @param $address
     * @param $last_name
     * @param $first_name
     * @param $father_name
     * @param $ploshad
     * @param $chislenost
     * @return bool
     */
    public function createPersonalAccount($address, $last_name, $first_name, $father_name, $ploshad, $chislenost)
    {
        $result = $this->db->prepare("INSERT INTO personal_accounts (address, last_name, first_name, father_name, ploshad, chislenost) VALUES (:address, :last_name, :first_name, :father_name, :ploshad, :chislenost)");
        return $result->execute([
            ':address' => $address,
            ':last_name' => $last_name,
            ':first_name' => $first_name,
            ':father_name' => $father_name,
            ':ploshad' => $ploshad,
            ':chislenost' => $chislenost,
        ]);
    }

    /**
     * @return array
     */
    public function getService(): array
    {
        return ($this->db->query('SELECT * FROM services'))->fetchAll();
    }

    /**
     * @return array
     */
    public function getPersonalAccount(): array
    {
        return ($this->db->query('SELECT * FROM personal_accounts'))->fetchAll();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteService(int $id): bool
    {
        return ($this->db->prepare("DELETE FROM services WHERE id = ?"))->execute([$id]);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deletePersonalAccount(int $id): bool
    {
        return ($this->db->prepare("DELETE FROM personal_accounts WHERE id = ?"))->execute([$id]);
    }


    /**
     * @return array
     */
    public function getPayment(): array
    {
        return ($this->db->query('SELECT *, SUM(summ) as amount FROM `payments` GROUP BY year, month'))->fetchAll();
    }

}