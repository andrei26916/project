<?php

require_once "layout/header.php";
require_once "../service/Main.php";
$services = (new Main())->getService();

if (!isset($_SESSION['role']))
{
    header('Location: /views/auth.php');
}

?>

<?php if ($_SESSION['role'] == 'admin'): ?>
    <div class="row">
        <form action="/controller/CreateService.php" method="GET" class="form-inline">
            <div class="form-group">
                <label for="exampleInputEmail1">Наименование</label>
                <input class="form-control" type="text" name="name" required>
            </div>
            <div class="form-group mb-2">
                <label for="exampleInputEmail1">Прайс</label>
                <input class="form-control" type="number" step="0.1" min="0" name="price" required>
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
    </div>
<?php endif; ?>

    <div class="row">
        <h1>Список услуг</h1>
        <?php foreach ($services as $service): ?>
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">
                            <?= $service['name'] ?>
                            <?php if ($_SESSION['role'] == 'admin'): ?>
                                <button style="float: right" onclick="del(<?= $service['id'] ?>)" type="button" class="btn btn-outline-danger btn-sm">X</button>
                            <?php endif; ?>
                        </h5>
                        <br>
                        <a style="float: right;" href="#" class="btn btn-success">Цена <?= $service['price'] ?> ₽</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<script>
    function del(id)
    {
        $.ajax({
            url: '/controller/DeleteService.php',         /* Куда пойдет запрос */
            method: 'get',             /* Метод передачи (post или get) */
            dataType: 'html',          /* Тип данных в ответе (xml, json, script, html). */
            data: {id: id},     /* Параметры передаваемые в запросе. */
            success: function(){   /* функция которая будет выполнена после успешного запроса.  */
                location.reload();
            }
        });
    }
</script>

<!--<form action="/controller/tets.php" method="GET">-->
<!--    <input type="text" name="test">-->
<!--    <button>go</button>-->
<!--</form>-->
</body>
</html>