<?php

require_once "layout/header.php";

?>

<?php if (isset($_SESSION['role'])):  ?>
    <div class="container">
        <div class="row">
            <form action="/controller/Auth.php" method="GET" class="form-inline">
                <h1>Привет <?= $_SESSION['role']?></h1>
                <button type="submit" class="btn btn-primary">Выход</button>
            </form>
        </div>
    </div>

<?php else:?>

    <div class="container">
        <div class="row">
            <form action="/controller/Auth.php" method="GET" class="form-inline">
                <div class="form-group">
                    <label for="exampleInputEmail1">Роль</label>
                    <select class="form-control" type="text" name="role" required>
                        <option value="admin">Начальник ЖКХ</option>
                        <option value="moder">Кассир</option>
                        <option value="user">Пользователь</option>
                    </select>
                </div>
                <br>
                <button type="submit" class="btn btn-primary">Авторизоваться</button>
            </form>
        </div>
    </div>

<?php endif; ?>

