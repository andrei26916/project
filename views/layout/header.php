<?php
session_start();
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/views/css/bootstrap.min.css" rel="stylesheet">
    <script src="/views/js/bootstrap.bundle.min.js" crossorigin="anonymous" type="application/javascript"></script>
    <script src="/views/js/bootstrap.js" crossorigin="anonymous" type="application/javascript"></script>
    <script src="/views/js/bootstrap.esm.min.js" crossorigin="anonymous" type="application/javascript"></script>
    <script src="/views/js/jquery-3.6.0.min.js" crossorigin="anonymous" type="application/javascript"></script>
    <title>Главная страница</title>
</head>
<body>
<!--<h1>Добро пожаловать</h1>-->


<div class="container">
    <div class="row">
        <img style="height: 350px;position: initial;" src="/views/img/banner.jpg" class="img-fluid" alt="" height="350">
        <div class="col">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container-fluid">
                    <a class="navbar-brand" href="/">MyHome</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="/views/service.php">Услуги</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/views/account.php">Квартиры</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/views/payment.php">Оплата</a>
                            </li>
                            <?php if (isset($_SESSION['role'])): ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="/controller/Auth.php">Выход</a>
                                </li>
                            <?php  endif; ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>

    <br>