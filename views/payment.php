<?php

require_once "layout/header.php";

if (!isset($_SESSION['role']))
{
    header('Location: /views/auth.php');
}

require_once "../service/Main.php";

$_monthsList = array(
    "1"=>"Январь","2"=>"Февраль","3"=>"Март",
    "4"=>"Апрель","5"=>"Май", "6"=>"Июнь",
    "7"=>"Июль","8"=>"Август","9"=>"Сентябрь",
    "10"=>"Октябрь","11"=>"Ноябрь","12"=>"Декабрь");

$payments = (new Main())->getPayment();

//die(var_dump($payments));

?>


<div class="container">
    <div class="row">
        <h1>Итог оплаты</h1>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Месяц</th>
                <th scope="col">Год </th>
                <th scope="col">Общая сумма</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($payments as $key => $payment): ?>
                <tr>
                    <th scope="row"><?= ++$key; ?></th>
                    <td><?= $_monthsList[$payment['month']]; ?></td>
                    <td><?= $payment['year']; ?></td>
                    <td><?= $payment['amount']; ?> ₽</td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>

    </div>
</div>
