<?php

require_once "layout/header.php";

if (!isset($_SESSION['role']))
{
    header('Location: /views/auth.php');
}

?>

    <div class="row">
        <div class="col-12">
            <h1>Информация</h1>
            <p>Сервис, позволяющий организациям, осуществляющим деятельность в сфере управления многоквартирными домами,
                самостоятельно создать сайт управляющей компании на основе данных, внесенных в автоматизированную
                информационную систему «Реформа ЖКХ», доработан в части возможности добавления электронной формы Единого
                портала государственных и муниципальных услуг "Сообщить о проблеме". Данная электронная форма
                предназначена для направления гражданами или юридическим лицами сообщений и обращений о проблемах в
                организации, в чьем ведение находится решение проблемы.</p>
        </div>
    </div>

    <div class="row">
        <h1>Новости</h1>
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="..." class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<form action="/controller/tets.php" method="GET">-->
<!--    <input type="text" name="test">-->
<!--    <button>go</button>-->
<!--</form>-->
</body>
</html>