<?php

require_once "layout/header.php";

if (!isset($_SESSION['role']))
{
    header('Location: /views/auth.php');
}

require_once "../service/Main.php";
$personalAccounts = (new Main())->getPersonalAccount();

?>

<?php if ($_SESSION['role'] == 'admin'): ?>
    <div class="row">
        <form action="/controller/CreatePersonalAccount.php" method="GET" class="form-inline">
            <div class="form-group">
                <label for="exampleInputEmail1">Адресс проживания</label>
                <input class="form-control" type="text" name="address" required>
            </div>
            <div class="form-group mb-2">
                <label for="exampleInputEmail1">Фамилия</label>
                <input class="form-control" type="text" name="last_name" required>
            </div>
            <div class="form-group mb-2">
                <label for="exampleInputEmail1">Имя</label>
                <input class="form-control" type="text" name="first_name" required>
            </div>
            <div class="form-group mb-2">
                <label for="exampleInputEmail1">Отчество</label>
                <input class="form-control" type="text" name="father_name" required>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Площадь</label>
                <input class="form-control" type="number" step="0.1" min="20" max="300" name="ploshad" required>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Численность</label>
                <input class="form-control" type="number" name="chislenost" min="1" max="20" required>
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
    </div>
<?php endif; ?>

    <div class="row">
        <h1>Список квартир</h1>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">ФИО</th>
                <th scope="col">Адресс </th>
                <th scope="col">Площадь</th>
                <th scope="col">Численность</th>
                <?php if ($_SESSION['role'] == 'admin'): ?>
                    <th scope="col"></th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($personalAccounts as $key=>$personalAccount): ?>
                <tr>
                    <th scope="row"><?= ++$key ?></th>
                    <td><?= ucfirst($personalAccount['last_name'] .' '.$personalAccount['first_name'] . ' ' . $personalAccount['father_name']); ?></td>
                    <td><?= $personalAccount['address'] ?></td>
                    <td><?= $personalAccount['ploshad'] ?></td>
                    <td><?= $personalAccount['chislenost'] ?></td>
                    <?php if ($_SESSION['role'] == 'admin'): ?>
                        <td><button onclick="del(<?= $personalAccount['id'] ?>)" type="button" class="btn btn-outline-danger btn-sm">X</button></td>
                    <?php endif; ?>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div>

</div>

<script>
function del(id)
{
    $.ajax({
        url: '/controller/DeletePersonalAccount.php',         /* Куда пойдет запрос */
        method: 'get',             /* Метод передачи (post или get) */
        dataType: 'html',          /* Тип данных в ответе (xml, json, script, html). */
        data: {id: id},     /* Параметры передаваемые в запросе. */
        success: function(){   /* функция которая будет выполнена после успешного запроса.  */
            location.reload();
        }
    });
}
</script>

<!--<form action="/controller/tets.php" method="GET">-->
<!--    <input type="text" name="test">-->
<!--    <button>go</button>-->
<!--</form>-->
</body>
</html>